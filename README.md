# DJANGO - MOVIE

![forthebadge](http://forthebadge.com/images/badges/made-with-python.svg)
![forthebadge](http://forthebadge.com/images/badges/built-with-love.svg)


1. Models 

`movie` used `one-to-many` to `mpaa_rating`<br/>
`movie` used `many-to-many` to `genre`

```sh
# MODELS

class MpaaRating(models.Model):
     type = models.CharField(max_length=25)
     label = models.CharField(max_length=150)

     def __str__(self) :
          return f"{self.type} {self.label}"
     
     def natural_key(self):
          return dict(
               type=self.type,
               text=self.label,
          )
     
class Genre(models.Model):
     name = models.TextField(max_length=150)

     def __str__(self) :
          return f"{self.name}"
     
     def natural_key(self):
        return self.name
     
class Movies(models.Model):
     name = models.TextField(blank= True)
     descriptions = models.TextField(blank= True)
     img_path = models.TextField(blank= True)
     duration = models.IntegerField(blank= True, validators=[MinValueValidator(1), MaxValueValidator(150)])
     genre = models.ManyToManyField(Genre)
     language = models.CharField(max_length= 50,null= True)
     user_rating = models.IntegerField(blank= True, validators=[MinValueValidator(1), MaxValueValidator(5)])
     slug = models.SlugField(default="", blank=True, null=False, db_index=True)
     mpaa_rating = models.ForeignKey(MpaaRating,on_delete=models.CASCADE, null=True)
     
     def get_absolute_url(self):
         Movies.objects.get(name=self.name).save()
         return reverse("movie-detail", args=[self.slug])
     
     def save(self, *args, **kwargs):
          self.slug = slugify(self.name)
          super().save(*args, **kwargs)

     def __str__(self) :
          return "%s %s %s %s %s %s %s" % (
               self.name,
               self.genre,
               self.descriptions,
               self.img_path,
               self.duration,
               self.language,
               self.user_rating,
        )
```

2. Json Result
<p>Search result json base on : https://drive.google.com/open?id=18HZ_bqk-oMIUdU8HIP07KX86AEVV0KY0 </p><br/>

<b>Evidence Screeenshoot :</b><br/>
![evidence json](static/evidence/Screenshot 2024-02-08 at 21.16.23.png)
<br/>
<b>Evidence Json :</b><br/>
```
[
  {
      "name": "Marvel's Captain America: Civil War",
      "user_rating": 5,
      "genre": [
          "Action",
          "Adventure"
      ],
      "mpaa_rating": {
          "type": "PG",
          "label": "Some Violence"
      },
      "slug": "marvels-captain-america-civil-war",
      "img_path": "assets/images/civilwar.png",
      "duration": 148
  },
  {
      "name": "Money Monster",
      "user_rating": 3,
      "genre": [
          "Comedy",
          "Adventure"
      ],
      "mpaa_rating": {
          "type": "PG",
          "label": "Some Intense Sequences"
      },
      "slug": "money-monster",
      "img_path": "assets/images/moneymonster.png",
      "duration": 99
  },
  {
      "name": "Twenty Four",
      "user_rating": 4,
      "genre": [
          "Thriller",
          "Action",
          "Adventure"
      ],
      "mpaa_rating": {
          "type": "M18",
          "label": "Sexual Scene and Coarse Language"
      },
      "slug": "twenty-four",
      "img_path": "assets/images/twentyfour.png",
      "duration": 143
  },
  {
      "name": "Bad Neighbours 2",
      "user_rating": 2,
      "genre": [
          "Action",
          "Comedy"
      ],
      "mpaa_rating": {
          "type": "R21",
          "label": "Sexual Content & Drug Use"
      },
      "slug": "bad-neighbours-2",
      "img_path": "assets/images/badneighbours.png",
      "duration": 110
  },
  {
      "name": "Disney's The Jungle Book",
      "user_rating": 4,
      "genre": [
          "Thriller",
          "Action"
      ],
      "mpaa_rating": {
          "type": "PG",
          "label": "Some Violence"
      },
      "slug": "disneys-the-jungle-book",
      "img_path": "assets/images/junglebook.png",
      "duration": 106
  }
]
```

<b>Evidence Home :</b><br/>
![evidence home](static/evidence/Screenshot 2024-02-08 at 17.04.39.png)
<br/>

<b>Evidence Detail :</b><br/>
![evidence detail](static/evidence/Screenshot 2024-02-08 at 17.04.48.png)
<br/>

[(Back to top)](#django-movie)




