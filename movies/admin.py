from django.contrib import admin
from .models import Movies,Genre,MpaaRating

class MoviesAdmin(admin.ModelAdmin):
     prepopulated_fields = {"slug":("name",)}
     list_display = ("name", "user_rating","duration" )
     list_filter = ("name", "user_rating","duration" )

class GenreAdmin(admin.ModelAdmin):
     list_display = ("name",)
     list_filter = ("name", )

class MpaaRatingAdmin(admin.ModelAdmin):
     list_display = ("type","label")
     list_filter = ("type","label" )

admin.site.register(Movies, MoviesAdmin)
admin.site.register(Genre, GenreAdmin)
admin.site.register(MpaaRating, MpaaRatingAdmin)


