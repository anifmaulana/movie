from django.db import models
from django.core import validators
from django.core.validators import MinValueValidator, MaxValueValidator
from django.urls import reverse
from django.utils.text import slugify

# Create your models here.

class MpaaRating(models.Model):
     type = models.CharField(max_length=25)
     label = models.CharField(max_length=150)

     def __str__(self) :
          return f"{self.type} {self.label}"
     
     def natural_key(self):
          return dict(
               type=self.type,
               text=self.label,
          )
     
class Genre(models.Model):
     name = models.TextField(max_length=150)

     def __str__(self) :
          return f"{self.name}"
     
     def natural_key(self):
        return self.name
     
class Movies(models.Model):
     name = models.TextField(blank= True)
     descriptions = models.TextField(blank= True)
     img_path = models.TextField(blank= True)
     duration = models.IntegerField(blank= True, validators=[MinValueValidator(1), MaxValueValidator(150)])
     genre = models.ManyToManyField(Genre)
     language = models.CharField(max_length= 50,null= True)
     user_rating = models.IntegerField(blank= True, validators=[MinValueValidator(1), MaxValueValidator(5)])
     slug = models.SlugField(default="", blank=True, null=False, db_index=True)
     mpaa_rating = models.ForeignKey(MpaaRating,on_delete=models.CASCADE, null=True)
     
     def get_absolute_url(self):
         Movies.objects.get(name=self.name).save()
         return reverse("movie-detail", args=[self.slug])
     
     def save(self, *args, **kwargs):
          self.slug = slugify(self.name)
          super().save(*args, **kwargs)

     def __str__(self) :
          return "%s %s %s %s %s %s %s" % (
               self.name,
               self.genre,
               self.descriptions,
               self.img_path,
               self.duration,
               self.language,
               self.user_rating,
        )