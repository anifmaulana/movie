from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path('movie-detail/<slug:slug>', views.detail, name="movie-detail"),
    path('search/', views.search, name="search"),
]
