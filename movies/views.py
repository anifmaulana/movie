import json
from django.shortcuts import get_object_or_404, render
from django.core import serializers
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.forms.models import model_to_dict

from .models import Movies

# Create your views here.
def index(request):
     movies = Movies.objects.all()
     return render(request, "movies/index.html", {
          "movies": movies
      })

def detail(request, slug):
     # try:
     #      movies = Movies.objects.get(slug=slug)
     # except:
     #      raise Http404()
     movies = get_object_or_404(Movies, slug=slug)
     return render(request, "movies/detail.html", {
          "name": movies.name,
          "genre": movies.genre,
          "img_path": movies.img_path,
          "descriptions": movies.descriptions,
          "mpaa_rating": movies.mpaa_rating,
          "duration": movies.duration,
          "language": movies.language,
          "user_rating": movies.user_rating,
      })

@csrf_exempt
def search(request):
     value = request.POST.get("param")

     filter_movie = Movies.objects.all()
     if value:
          filter_movie = Movies.objects.filter(name__contains= value)                    
    
     custom_field =[]
     for v in filter_movie:
          genre = []
          for x  in v.genre.all():
               genre.append(x.name)
          custom_field.append({'name': v.name, 'user_rating': v.user_rating,'genre':genre, 'mpaa_rating': model_to_dict(v.mpaa_rating, fields="label, type"), 'slug': v.slug, 'img_path': v.img_path, 'duration': v.duration})
     
     return HttpResponse(json.dumps(custom_field))